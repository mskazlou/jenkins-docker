from pages.mainPage import MainPage
from pages.mainPage import FramePage


class TestPageObjectModel:
    """ Test suit"""

    def test_main_page(self, web_driver):
        """ Make sure we have access to correct page. """
        page = MainPage(web_driver)
        assert page.get_title() == 'The Internet'

    def test_checkbox(self, web_driver):
        """ Make sure the checkbox element is gone. """

        expected_text = "It's gone!"
        error_msg = "Can't find text!"

        page = MainPage(web_driver)
        page.checkbox.click()
        page.remove_button.click()

        missing_text = page.checkbox_gone_message.get_text()
        if page.checkbox.wait_to_be_not_clickable():    # wait_to_be_not_clickable method returns True
            assert missing_text == expected_text, error_msg
        else:
            assert False

    def test_input_field(self, web_driver):
        """ Make sure the input field is enabled after all manipulations. """

        page = MainPage(web_driver)

        if not page.input_field.is_enabled():
            page.enable_button.click()

        missing_text = page.enable_text.get_text()  # returns "it's enabled!" text

        if missing_text and page.input_field.is_enabled():
            assert True

        else:
            assert False


class TestFrames:

    def test_iframe(self, web_driver):
        """ Make sure text inside iframe is equal to expected result. """

        expected_text = 'Your content goes here.'

        page = FramePage(web_driver)
        page.link_to_frame.click()  # clicking on the link to the website with iframe_element

        frame_element = page.frame_element.find()   # returns iframe element

        page.switch_to_iframe(frame_element)

        text = page.inner_page_text.get_text()

        assert text == expected_text


