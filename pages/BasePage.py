from selenium.webdriver.remote.webdriver import WebDriver


class WebPage(object):
    """ don't understand what is going in dunder (getattribute, setattr) methods,
    but I'm going to figure that out soon. """

    _web_driver = None

    def __init__(self, web_driver: WebDriver, url=''):
        self._web_driver = web_driver
        self.get(url)

    def __setattr__(self, name, value):
        if not name.startswith('_'):
            self.__getattribute__(name)._set_value(self._web_driver, value)
        else:
            super(WebPage, self).__setattr__(name, value)

    def __getattribute__(self, item):
        attr = object.__getattribute__(self, item)

        if not item.startswith('_') and not callable(attr):
            attr._web_driver = self._web_driver
            attr._page = self

        return attr

    def get(self, url):
        self._web_driver.get(url)

    def switch_to_iframe(self, iframe):
        """ Switch to iframe by its name. """

        self._web_driver.switch_to.frame(iframe)

    def switch_out_iframe(self):
        """ Cancel iframe focus. """

        self._web_driver.switch_to.default_content()

    def get_title(self):
        """ Returns current title text. """

        return self._web_driver.title

