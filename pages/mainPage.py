# -*- encoding=utf8 -*-

from pages.BasePage import WebPage
from pages.webElement import WebElement


class MainPage(WebPage):

    def __init__(self, web_driver, url=''):
        if not url:
            url = 'http://the-internet.herokuapp.com/dynamic_controls'

        super().__init__(web_driver, url)

    checkbox = WebElement(css_selector='[type="checkbox"]')

    remove_button = WebElement(css_selector='[onclick="swapCheckbox()"]')

    checkbox_gone_message = WebElement(id="message")

    input_field = WebElement(css_selector="input[type='text']")

    enable_button = WebElement(css_selector='button[onclick="swapInput()"]')

    enable_text = WebElement(xpath='//form[contains(@id, "input-")]/p[@id="message"]')


class FramePage(WebPage):

    def __init__(self, web_driver, url=''):

        if not url:
            url = 'http://the-internet.herokuapp.com/frames'

        super().__init__(web_driver, url)

    link_to_frame = WebElement(css_selector='[href="/iframe"]')

    frame_element = WebElement(id="mce_0_ifr")

    inner_page_text = WebElement(xpath="//p[normalize-space() = 'Your content goes here.']")

    inner_input_field = WebElement(id="tinymce")
