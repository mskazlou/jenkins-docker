from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class WebElement(object):
    _locator = ('', '')
    _web_driver = None
    _page = None
    _timeout = 10

    def __init__(self, timeout=10, **kwargs):
        self._timeout = timeout

        for attr in kwargs:
            self._locator = (str(attr).replace('_', ' '), str(kwargs.get(attr)))

    def find(self, timeout=10):
        """ Returns element. """

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.presence_of_element_located(self._locator)
            )
        except:
            print('Element not found on the page!')

        return element

    def wait_to_be_clickable(self, timeout=10):
        """ Wait until the element will be ready for click. """

        element = None

        try:
            element = WebDriverWait(self._web_driver, timeout).until(
                EC.element_to_be_clickable(self._locator)
            )
        except:
            print('Element not clickable!')

        return element

    def wait_to_be_not_clickable(self, timeout=10):
        """ Wait until the element will be not ready for click. """
        element = None
        try:
            element = WebDriverWait(self._web_driver, timeout).until_not(
                EC.element_to_be_clickable(self._locator)
            )

        except:
            print('Element is still clickable!')

        return element

    def send_keys(self, keys):
        """ Send keys to the element. """

        element = self.find()

        if element:
            element.click()
            element.clear()
            element.send_keys(keys)

        else:
            msg = 'Element with {0} _locator not found'
            raise AttributeError(msg.format(self._locator))

    def click(self, hold_seconds=0, x_offset=1, y_offset=1):
        """ Wait if its needed and click the element. """

        element = self.wait_to_be_clickable()

        if element:
            action = ActionChains(self._web_driver)
            action.move_to_element_with_offset(element, x_offset, y_offset). \
                pause(hold_seconds).click(on_element=element).perform()
        # else:
        #     msg = 'Element with locator {0} not found'
        #     raise AttributeError(msg.format(self._locator))

    def get_text(self):
        """ Returns text of the element. """

        element = self.find()
        text = ''

        try:
            text = str(element.text)
        except Exception as e:
            print('Error: {0}'.format(e))

        return text

    def get_attribute(self, attribute_name):
        """ Returns attribute's value of the element. """

        element = self.find()

        if element:
            return element.get_attribute(attribute_name)

    def is_enabled(self):
        """ Checking if element is enabled. Returns boolean value. """
        element = self.find(timeout=4)

        try:
            return element.is_enabled()

        except Exception as e:
            print('Error {0}'.format(e))






