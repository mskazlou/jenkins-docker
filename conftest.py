from selenium import webdriver
import pytest
from selenium.webdriver.chrome.service import Service
from pathlib import Path
from selenium.webdriver.chrome.options import Options


chrome_driver_path = Path('/home/mkazlou/chromedriver/chromedriver')
s = Service(chrome_driver_path)
chrome_options = Options()

chrome_options.add_argument("--disable-extensions")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--headless")


@pytest.fixture(scope='class')
def web_driver():
    web_driver = webdriver.Chrome(service=s, options=chrome_options)
    web_driver.maximize_window()
    # web_driver.implicitly_wait(4)

    yield web_driver

    web_driver.quit()
    print('\nquit browser!')

